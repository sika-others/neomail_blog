---
layout: post
title: Spusteni verze 0.2 s redirecty
---

Verze 2 prinasi redirecty emailu, zatim se nedaji nastavit v UI, ale zanedloho to pujde.

Redirecty se nastavuji k jednotlivim accountum, to znamena __sika@neomail.cz__ spravuje redirecty pro vsechny boxy. Dale je pak mozno spravovat redirecty cele domeny specialnim uctem __*@neomail.cz__.

Redirecty se nastavuji Python regulernimi vyrazy, ktere si muzete vyzkouset zde <http://pythex.org/>.

Pro presmerovani vsech emailu staci zadat __.*__. Pro presmerovani pouze 2 slozek (neomail, bitcoin) pouzijte tento vyraz __sika\\+(neomail\|bitcoin)@neomail.cz__.

