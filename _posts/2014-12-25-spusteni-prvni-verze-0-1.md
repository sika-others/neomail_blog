---
layout: post
title: Spusteni prvni verze 0.1
---

Dnes jsme spusti prvni verzi naseho SMTP serveru s jednoduchym (a vsak znacne nedodelanym webovym UI). Server bezi na Python/Twisted a jako databazi pouzivame PostgreSQL. Mame napsany vlastni server pro prijimani posty, pro odesilani pouzivame exim4. Proto take mame v DNS 2 zaznamy o SMTP serverech, jeden pro prichozi postu (incoming-smtp.neomail.cz) a jeden pro odchozi (outgoing-smtp.neomail.cz).

Nas server zatim IMAP nepodporuje, ale tohoto zasadniho nedostatku jsme si vedomi a co nejdrive jej doimplementujeme.

Webove UI zatim umi jen cist emaily v zdrojovem kodu a odesilat emaily.

Server od zacatku pocita se snadnym tridenim mailu do slozek staci poslat mail na __sika+neomail@neomail.cz__ a email se automaticky vlozi do slozky __neomail__. Pokud slozka neexistuje tak se sama vytvori.

Jako jedna z hlavnich vyhod Neomailu je nativni podpora ruznych domen s dobrou cenovou politikou pro emaily na vice domenach. Cenik zatim pripravujeme.

Mame take online podporu na <support@neomail.cz>, kde si muzete zazadat o zalozeni uctu, registrace jeste neni hotova.

